/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexiones;

import Modelos.Cobros;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Wilb928
 */
@Stateless
public class CobrosFacade extends AbstractFacade<Cobros> {
    @PersistenceContext(unitName = "ExamenSWPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CobrosFacade() {
        super(Cobros.class);
    }
    
}
